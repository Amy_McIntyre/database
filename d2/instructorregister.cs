﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace d2
{
    //Student Name: Amy McIntyre
    //Student ID: 9999413
    public partial class instructorregister : Form
    {
        public instructorregister()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string username = "", password = "", firstname = "", lastname = "", email = "", phone = "";

            bool hasText = checkTextBoxes();
            if (!hasText)
            {
                MessageBox.Show("Please make sure all textboxes have text.  Middle Name is not compulsory");
                textBox1.Focus();
                return;
            }

            try
            {
                username = textBox1.Text.Trim();
                password = textBox2.Text.Trim();
                firstname = textBox3.Text.Trim();
                lastname = textBox4.Text.Trim();
                email = textBox5.Text.Trim();
                phone = textBox6.Text.Trim();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            try
            {
                SQL.executeQuery("INSERT INTO INSTRUCTOR VALUES ('" + username + "', '" + password + "', '" + firstname + "', '" + lastname + "', '" + email + "', '" + phone + "' )");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Register attempt unsuccessful.  Check insert statement.  Could be a Username conflict too.");
                return;
            }
            
            MessageBox.Show("Successfully Registered: " + firstname + " " + lastname + ". Your username is: " + username);
        }

        private bool checkTextBoxes()
        {
            bool holdsData = true;
            foreach (Control c in this.Controls)
            {
                if (c is TextBox && (c != textBox5))
                {
                    if ("".Equals((c as TextBox).Text.Trim()))
                    {
                        holdsData = false;
                    }
                }
            }
            return holdsData;
        }

            private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            instructor register = new instructor();
            register.ShowDialog();
            this.Close();
        }

        private void instructorregister_Load(object sender, EventArgs e)
        {

        }
    }
}
