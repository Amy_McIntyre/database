﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace d2
{
    //Student Name: Amy McIntyre
    //Student ID: 9999413
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            textBoxpass.PasswordChar = '*';

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool loggedIn = false;
            string username = "", password = "", firstname = "", lastname = "";
            
            if ("".Equals(textBoxuser.Text.Trim()) || "".Equals(textBoxpass.Text.Trim()))
            {
                MessageBox.Show("Please make sure you enter a Username and Password");
                return;
            }


            try
            {

                username = textBoxuser.Text.Trim();
                password = textBoxpass.Text.Trim();
            }
            catch (Exception ex)
            {
  
                MessageBox.Show("Username or Password given is in an incorrect format.");
                return;
            }

            SQL.selectQuery("select * FROM Client");

            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (username.Equals(SQL.read[0].ToString()) && password.Equals(SQL.read[1].ToString()))
                    {
                        loggedIn = true;
                        firstname = SQL.read[2].ToString();
                        lastname = SQL.read[3].ToString();
                        break;
                    }
                }
            }
            else
            {
                MessageBox.Show("There are no accounts registed");
                return;
            }

            if (loggedIn)
            {
                MessageBox.Show("Successfully logged in as: " + firstname + " " + lastname);
            }
            else
            {
                MessageBox.Show("Login attempt unsuccessful! Please check details");
                textBoxuser.Focus();
                return;
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Welcome register = new Welcome();
            register.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            userregister register = new userregister();
            register.ShowDialog();
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBoxuser_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxpass_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
