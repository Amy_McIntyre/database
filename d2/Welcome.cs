﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace d2
{
    public partial class Welcome : Form
    {
        public Welcome()
        {
            InitializeComponent();
        }

        private void username_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
                this.Hide();
                userregister register = new userregister();
                register.ShowDialog();
                this.Close();
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login register = new Login();
            register.ShowDialog();
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            instructor register = new instructor();
            register.ShowDialog();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Admin register = new Admin();
            register.ShowDialog();
            this.Close();
        }
    }
}
